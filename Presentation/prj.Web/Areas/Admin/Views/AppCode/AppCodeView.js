﻿ $(function() {

     function editAppCode(appCodeId) {
         var appCodeDialog = new BootstrapDialog({
                 title: "New App Code",
                 buttons: [{
                     label: " Save",
                     cssClass: "btn-primary",
                     icon: "fa fa-save",
                     action: function(dialog) {
                         if ($('#formEditAppCode').valid()) {
                             $.ajax({
                                 type: "POST",
                                 url: $('#addAppCode').attr("data-url"),
                                 data: $('#formEditAppCode').serialize(),
                                 success: function(result) {
                                     if (result.success) {
                                         reloadGrid();
                                         dialog.close();
                                         populateTypes();
                                     } else {
                                         $('#alert').html(nmbl.alert.error("Error", result.data));
                                     }
                                 },
                             });
                         }
                         return false;
                     }
                }, {
                    label: "Cancel",
                    action: function (dialog) {
                        dialog.close();
                    }
                 }]
             });

         appCodeDialog.realize();

         appCodeDialog.getModalBody().load($('#addAppCode').attr("data-url") + "?" + $.param({ appCodeId: appCodeId }),
             function() {
                 appCodeDialog.open();
                 $.validator.unobtrusive.parse($(appCodeDialog.getModalBody()));
             });

         if (appCodeId == 0) {
             appCodeDialog.setTitle("Add Code");
         } else {
             appCodeDialog.setTitle("Edit Code");
         }

         return false;
     }
     
     $('#addAppCode').click(function() {
         editAppCode(0);
     });

     $('#appCodeGrid').on('click', '.modifyAppCode', function() {
         var appCodeId = $(this).attr('data-value');
         editAppCode(appCodeId);
         return false;
     });

     $('#appCodeGrid').on('click', '.deleteAppCode', function() {
         if (confirm("Would you like delete the app code?")) {
             var appCodeId = $(this).attr('data-value');
             $.ajax({
                 type: "POST",
                 url: $('#data-url-delete').val(),
                 data: { appCodeId: appCodeId },
                 success: function(result) {
                     if (result.success) {
                         reloadGrid();
                         populateTypes();
                     } else {
                         $('#alertContainer').html(nmbl.alert.error("Error", result.data));
                     }
                 }
             });
         }
         return false;
     });

     $('#chkCodeTypes').change(function() {
         reloadGrid();
     });

     function reloadGrid() {
         var oTable = $('#appCodeGrid').dataTable();
         nmbl.datatable.RefreshGrid(oTable);
     }

     $('#chkActive').change(function() {
         reloadGrid();
     });
     
     function populateTypes() {
         var url = $('#chkCodeTypes').attr("action");
         var CodeTypes = $('#chkCodeTypes').val();
        $('#chkCodeTypes').val([]);
        $.ajax({
            type: 'POST',
            url: url,
            dataType: 'json',
            async: false,
            success: function(result) {
                var ddlCode = $('#chkCodeTypes');
                $('option', ddlCode).remove();

                $.each(result, function() {
                    ddlCode.append($('<option />').val(this.Value).text(this.Text));
                });
            }
        });
        $('#chkCodeTypes').val(CodeTypes); 
    }
 });